;
; R�f�rences syst�me
;

;
; Types de fichiers
;

DSK_FILE_TYPE_PROTECTED_FLAG        Equ 1
DSK_FILE_TYPE_VERSION0_FLAG         Equ (0<<4)
DSK_FILE_TYPE_VERSION1_FLAG         Equ (1<<4)

DSK_FILE_TYPE_BASIC                 Equ &0+dsk_file_type_version0_flag
DSK_FILE_TYPE_BASIC_PROTECTED       Equ &0+dsk_file_type_version0_flag+dsk_file_type_protected_flag
DSK_FILE_TYPE_BINARY                Equ &2+dsk_file_type_version0_flag
DSK_FILE_TYPE_BINARY_PROTECTED      Equ &2+dsk_file_type_version0_flag+dsk_file_type_protected_flag
DSK_FILE_TYPE_SCREEN_IMAGE          Equ &4+dsk_file_type_version0_flag
DSK_FILE_TYPE_ASCII                 Equ &6+dsk_file_type_version1_flag
DSK_FILE_TYPE_SYMBOLIC_LINK         Equ &a+dsk_file_type_version0_flag  ; Nouveau type sp�cifique AlbiDOS

;
; Modes d'acc�s aux fichiers
;

DSK_FILE_MODE_PENDING               Equ &00
DSK_FILE_MODE_CHAR                  Equ &01
DSK_FILE_MODE_DIRECT                Equ &02
DSK_FILE_MODE_STREAM                Equ &03         ; Nouveau mode sp�cifique AlbiDOS

;
; Donn�es de gestion disque
;

DSK_RETRY_COUNT                     Equ &be66       ; Tentatives en lecture/�criture disque (&10 par d�faut)
DSK_MESSAGE_STATUS                  Equ &be78       ; �tat d'activation des messages Amsdos (&00 = on, &ff = off)

;
; Codes d'erreur disque
;

DSK_ERR_USER_HAS_HIT_ESCAPE         Equ &00
DSK_ERR_BAD_CHANNEL_STATE           Equ &0e
DSK_ERR_HARD_END_OF_FILE            Equ &0f
DSK_ERR_BAD_COMMAND                 Equ &10
DSK_ERR_FILE_ALREADY_EXISTS         Equ &11
DSK_ERR_FILE_NOT_FOUND              Equ &12
DSK_ERR_DIRECTORY_FULL              Equ &13
DSK_ERR_DISC_FULL                   Equ &14
DSK_ERR_DISC_CHANGED                Equ &15
DSK_ERR_FILE_IS_READ_ONLY           Equ &16
DSK_ERR_DIRECTORY_FOUND             Equ &17         ; Nouveau code sp�cifique AlbiDOS
DSK_ERR_FILE_IS_NOT_A_LINK          Equ &18         ; Nouveau code sp�cifique AlbiDOS
DSK_ERR_UNSUPPORTED_DRIVE           Equ &19         ; Nouveau code sp�cifique AlbiDOS
DSK_ERR_SOFT_END_OF_FILE            Equ &1a
DSK_ERR_NOT_A_DIRECTORY             Equ &1b         ; Nouveau code sp�cifique AlbiDOS
; Erreurs bas niveau (provoquent une requ�te DOS)
DSK_ERR_NO_MEDIA                    Equ &48
DSK_ERR_NO_DATA                     Equ &44
DSK_ERR_WRITE_PROTECTED_MEDIA       Equ &42

DSK_ERR_QUIET_FLAG                  Equ (1<<7)
DSK_ERR_FDC_FLAG                    Equ (1<<6)

DSK_ERR_QUIET_USER_HAS_HIT_ESCAPE   Equ dsk_err_quiet_flag+dsk_err_user_has_hit_escape
DSK_ERR_QUIET_BAD_CHANNEL_STATE     Equ dsk_err_quiet_flag+dsk_err_bad_channel_state
DSK_ERR_QUIET_HARD_END_OF_FILE      Equ dsk_err_quiet_flag+dsk_err_hard_end_of_file
DSK_ERR_QUIET_BAD_COMMAND           Equ dsk_err_quiet_flag+dsk_err_bad_command
DSK_ERR_QUIET_FILE_ALREADY_EXISTS   Equ dsk_err_quiet_flag+dsk_err_file_already_exists
DSK_ERR_QUIET_FILE_NOT_FOUND        Equ dsk_err_quiet_flag+dsk_err_file_not_found
DSK_ERR_QUIET_DIRECTORY_FULL        Equ dsk_err_quiet_flag+dsk_err_directory_full
DSK_ERR_QUIET_DISC_FULL             Equ dsk_err_quiet_flag+dsk_err_disc_full
DSK_ERR_QUIET_DISC_CHANGED          Equ dsk_err_quiet_flag+dsk_err_disc_changed
DSK_ERR_QUIET_FILE_IS_READ_ONLY     Equ dsk_err_quiet_flag+dsk_err_file_is_read_only
DSK_ERR_QUIET_DIRECTORY_FOUND       Equ dsk_err_quiet_flag+dsk_err_directory_found
DSK_ERR_QUIET_FILE_IS_NOT_A_LINK    Equ dsk_err_quiet_flag+dsk_err_file_is_not_a_link
DSK_ERR_QUIET_UNSUPPORTED_DRIVE     Equ dsk_err_quiet_flag+dsk_err_unsupported_drive
DSK_ERR_QUIET_SOFT_END_OF_FILE      Equ dsk_err_quiet_flag+dsk_err_soft_end_of_file
DSK_ERR_QUIET_NOT_A_DIRECTORY       Equ dsk_err_quiet_flag+dsk_err_not_a_directory
; Erreurs bas niveau (provoquent une requ�te DOS)
DSK_ERR_QUIET_NO_MEDIA              Equ dsk_err_quiet_flag+dsk_err_no_media
DSK_ERR_QUIET_NO_DATA               Equ dsk_err_quiet_flag+dsk_err_no_data
DSK_ERR_QUIET_WRITE_PROTECTED_MEDIA Equ dsk_err_quiet_flag+dsk_err_write_protected_media

;
; Commandes pour le vecteur BIOS CRTL-J de l'AlbiDOS
;

CMD_CAT_FAKED_PARENT                Equ 0
CMD_DEVICE_LIST                     Equ 1
CMD_DEVICE_INFO                     Equ 2
CMD_DISABLED_LINK                   Equ 3
CMD_EMULATE_SOFT_EOF                Equ 4
CMD_CAS_IN_READ                     Equ 5
CMD_CAS_IN_SEEK                     Equ 6
CMD_CAS_OUT_WRITE                   Equ 7
CMD_CAS_OUT_SEEK                    Equ 8

;
; Donn�es diverses
;

; Basic

_BAS_MEMORY                         Equ &ae5e       ; Valeur du memory (664/6128 seulement)

; Carte m�moire du kernel

_KL_ROM_BASE_ADDRESS_TABLE          Equ &b8da       ; 664/6128 seulement
_KL_ROM_BLOCK_CHAIN                 Equ &b8d3

; Carte m�moire du DOS

_DOS_ROM_TEMP_ROUTINE               Equ &b13b       ; Espace temporaire utilis� pour l'appel de routines externes (&24 octzts, 664/6128 seulement)
_DOS_ROM_BASE_ADDRESS               Equ &be7d       ; Adresse o� est stock�e l'adresse de la zone de travail de l'Amsdos

