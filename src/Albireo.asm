;
; AlbiDOS
;
; Gestionnaire de stockage de masse pour l'Albireo
; Par OffseT/Futurs' - (c) 2016-2020
;
; Pr�vu pour �tre assembl� avec rasm 1.2+
;

        Save"bin/Albireo.rom",codestart,codeend-codestart,amsdos

        Read"SystemCalls.i"
        Read"SystemData.i"
        Read"Devices.i"
        Read"ConfigFile.i"
        Read"CH376.i"

        Read"DataMap.i"

        Org &c000

CodeStart       db 1    ; Extension ROM

                db 0    ; Mark
                db 9    ; Version
                db 8    ; Revision

_RsxTable       dw rsxtable

                ; RSX de base de l'AMSDOS
_InitRom        jp initrom
_DOS            jp rsx_dos
_Disc           jp rsx_disc
_Disc_In        jp rsx_disc_in
_Disc_Out       jp rsx_disc_out
_Tape           jp rsx_tape
_Tape_In        jp rsx_tape_in
_Tape_Out       jp rsx_tape_out
_A              jp rsx_a
_B              jp rsx_b
_Drive          jp rsx_drive
_Path           jp rsx_path
_Dir            jp rsx_dir
_Era            jp rsx_era
_Ren            jp rsx_ren
                ; BIOS de base de l'AMSDOS
_ctrl_a         jp bios_setmessage
_ctrl_b         jp bios_setupdisc
_ctrl_c         jp bios_selectformat
_ctrl_d         jp bios_readsector
_ctrl_e         jp bios_writesector
_ctrl_f         jp bios_formattrack
_ctrl_g         jp bios_movetrack
_ctrl_h         jp bios_getstatus
_ctrl_i         jp bios_setretrycount
                ; CP/M 2
                jp bios_void ; cpm_enter_firmware
                jp bios_void ; cpm_set_reg_save
                jp bios_void ; cpm_set_sio
                jp bios_void ; cpm_setcmnd_buffer
                jp bios_void ; cpm_do_in_status
                jp bios_void ; cpm_do_in
                jp bios_void ; cpm_do_out_status
                jp bios_void ; cpm_do_out
                jp bios_void ; cpm_d1_in_status
                jp bios_void ; cpm_d1_in
                jp bios_void ; cpm_d1_out_status
                jp bios_void ; cpm_di_out
                 ; Menu du firmware v4
                jp firm_plusmenu
                ; BIOS additionnelles de l'AlbiDOS
_ctrl_j         jp bios_command
_ctrl_z         jp rsx_dos
                ; RSX additionnelles de l'AlbiDOS
_Protect        jp rsx_protect
_Unprotect      jp rsx_unprotect
_Hide           jp rsx_hide
_Show           jp rsx_show
_Info           jp rsx_info

RSXTable
                ; RSX de base de l'AMSDOS
_RSX_AlbiDOS    str "AlbiDOS"
_RSX_DOS        str "DOS"  ; Remplace |CPM
_RSX_DISC       str "DISC"
_RSX_DISC_IN    str "DISC.IN"
_RSX_DISC_OUT   str "DISC.OUT"
_RSX_TAPE       str "TAPE"
_RSX_TAPE_IN    str "TAPE.IN"
_RSX_TAPE_OUT   str "TAPE.OUT"
_RSX_A          str "A"
_RSX_B          str "B"
_RSX_DRIVE      str "DRIVE"
_RSX_PATH       str "PATH" ; Remplace |USER
_RSX_DIR        str "DIR"
_RSX_ERA        str "ERA"
_RSX_REN        str "REN"
                ; BIOS de base de l'AMSDOS
_RSX_CTRL_A     db &81    ; Ctrl A
_RSX_CTRL_B     db &82    ; Ctrl B
_RSX_CTRL_C     db &83    ; Ctrl C
_RSX_CTRL_D     db &84    ; Ctrl D
_RSX_CTRL_E     db &85    ; Ctrl E
_RSX_CTRL_F     db &86    ; Ctrl F
_RSX_CTRL_G     db &87    ; Ctrl G
_RSX_CTRL_H     db &88    ; Ctrl H
_RSX_CTRL_I     db &89    ; Ctrl I
                ; CP/M
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                db &80
                ; Menu du firmware v4
                db &80
                ; BIOS additionnelles de l'AlbiDOS
_RSX_CTRL_J     db &8a    ; Ctrl J
_RSX_CTRL_Z     db &9a    ; Ctrl Z
                ; RSX addtionnelles de l'AlbiDOS
_RSX_PROTECT    str "PROTECT"
_RSX_UNPROTECT  str "UNPROTECT"
_RSX_HIDE       str "HIDE"
_RSX_SHOW       str "SHOW"
_RSX_INFO       str "INFO"
                ; End of RSX table
                db 0

;
; Erreur d'initialisation
;

InvalidInstall
        call beep
        ld hl,msg_init_error
        call printstring
        jp skipinit

;
; Initialise la ROM
;

InitROM push ix
        push iy

        push hl
        push de

; D�sactivation si shift seul est appuy�

        call checkqualifiers        ; A = �tat de shift (5) et control (7) (actif 0)
        cp &80
        jp z,skipinit

; V�rification de la pr�sence de l'Albireo
; -> en sortie A=r�vision

        call checkalbireo
        jp nz,skipinit

; Est-ce qu'une ROM disque compatible Amsdos est d�j� en place ?

        ld e,a                      ; E = r�vision de l'Albireo
        push de
        call findamsdos             ; C = num�ro de la ROM AMSDOS
        pop de
        ld b,e                      ; B = r�vision de l'Albireo
        jr c,integratedinstall

; Peut-�tre que la ROM est l� mais non initialis�e ?

        ld c,0                      ; C = no AMSDOS
        push bc
        call huntamsdos
        ld a,c
        pop bc
        jr nc,standaloneinstall

; On initialise manuellement la ROM AMSDOS trouv�e

AmsdosManualInstall
        ld c,a
        call _kl_curr_selection
        cp c
        jr nc,invalidinstall

        pop de
        pop hl
        push bc
        call _kl_init_back
        pop bc
        push hl
        push de
        jr nc,standaloneinstall

; Installation � la place de l'Amsdos

IntegratedInstall
        call _kl_curr_selection
        ld e,a
        ld d,0
        ld ix,_kl_rom_base_address_table
        add ix,de
        add ix,de
        ld de,(_dos_rom_base_address)
        ld (ix+0),e                 ; Mise en place de l'adresse de travail
        ld (ix+1),d

        call _kl_curr_selection     ; Recherche de la position o� ins�rer notre ROM tag
        ld ix,_kl_rom_block_chain
NextROMBlockLoop
        ld e,(ix+0)
        ld d,(ix+1)
        cp (ix+2)
        jr c,romtagfound
NoMatchRomBlock
        ld ixl,e
        ld ixh,d
        jr nextromblockloop

ROMTagFound
        ld iy,(_dos_rom_base_address)

        ld a,(ix+2)                 ; Cr�ation de notre ROM tag
        GETPTR_HL romtag
        ld (hl),e
        inc hl
        ld (hl),d
        inc hl
        ld (hl),a
        inc hl
        ld (hl),0
        dec hl
        dec hl
        dec hl
        ex de,hl

        call _kl_curr_selection      ; Cha�nage de notre ROM tag
        ld (ix+0),e
        ld (ix+1),d
        ld (ix+2),a

        call configuredataspace
        ld de,msg_init_integrated
        call c,displayinitmsg        ; Affichage de l'invite uniquement au premier d�marrage

; On ne se d�clare pas au syst�me

SkipInit
        pop de
        pop hl
        pop iy
        pop ix

        xor a ; NC
        ret

; Installation ind�pendante

StandaloneInstall
        pop de
        pop hl

; R�servation de l'espace m�moire de travail

        push bc
        ld bc,-datasize
        add hl,bc
        pop bc

; Installation de l'espace m�moire

        push hl
        push de

        inc hl:push hl:pop iy

        ld (_dos_rom_base_address),iy

        call configuredataspace
        ld de,msg_init_standalone
        call c,displayinitmsg        ; Affichage de l'invite uniquement au premier d�marrage

        pop de
        pop hl
        pop iy
        pop ix

        scf
        ret

; Affiche le message d'invite de l'AlbiDOS
;
; Entr�e - DE = pointeur vers msg_init_integrated ou msg_init_standalone
; Sortie - N/A
; Alt�r� - AF,DE,HL

DisplayInitMsg
        ld hl,msg_init_albidos
        call printstring
        ex de,hl
        call printstring
        ex de,hl
        ld a,164
        call _txt_output
        ld hl,msg_init_copyright
        jp printstring

; Configure l'espace de donn�es de la ROM
;
; Entr�e - IY = Adresse de la zone m�moire de la ROM
;           B = R�vision du ch376 de l'Albireo
;           C = Num�ro de ROM o� se trouve l'Amsdos
; Sortie - C si c'est une premi�re init
;          NC si c'est une r�initialisation
; Alt�r� - AF,BC,DE,HL

ConfigureDataSpace
        PUT_C amsdosrom
        PUT_B albireorevision

        call savetapejumpblock

        call checkqualifiers        ; A = �tat de shift (5) et control (7) (actif 0)
ConfigDataSpaceAgain
        cp &20                      ; Suppression de la persistance si control seul est appuy�
        call z,beep
        call z,deleteconfig
        call initdata
        ld c,sc_indirectionf+sc_currentlogicaldrivef+sc_currentpathsf
        call loadconfig             ; On se synchronize sur les derni�res indirections sauv�es
        jr nz,installotherindirections
        call installindirections
        jr configuredone
InstallOtherIndirections
        cp 1
        jr nz,configuredone
        call rsx_tape
ConfigureDone
        jp checkreboot

;
; Initialise l'espace m�moire de travail
;
; Entr�e - IY = Adresse de la zone m�moire de la ROM
; Sortie - N/A
; Alt�r� - AF
;

InitData
        PUT_8 currentlogicaldrive,0
        PUT_8 currentuser,0
        PUT_8 currentphysicaldrivea,defaultphysicaldrive_a
        PUT_8 currentpatha,0
        PUT_8 currentphysicaldriveb,defaultphysicaldrive_b
        PUT_8 currentpathb,0

        PUT_8 fileindrive,&ff
        PUT_8 fileoutdrive,&ff
        PUT_8 examinedirdrive,&ff

        PUT_8 optionflags,0

        PUT_8 txtoutputjump,0
        PUT_8 lowlevelfile,llf_kindnone
        PUT_8 lowlevelmount,&ff

        ld a,&10
        ld (dsk_retry_count),a
        ld a,0
        ld (dsk_message_status),a

        ret

;
; Installation des indirections
;
; Entr�e - IY = Adresse de la zone m�moire de la ROM
; Sortie - N/A
; Alt�r� - AF,BC,DE,HL
;

InstallIndirections
        call _kl_curr_selection         ; A  = ROM de la routine d'indirection
        ld de,cas_vectordispatcher      ; DE = Adresse de la routine d'indirection

        GETPTR_HL faraddress
        ld b,h
        ld c,l                          ; BC = Adresse des donn�es de l'indirection

        ld (hl),e                       ; Mise en place des donn�es "far address"
        inc hl
        ld (hl),d
        inc hl
        ld (hl),a

        ld hl,_cas_in_open              ; HL = Premi�re indirection
        ld a,13                         ; A  = Nombre d'indirections
LoopInstallRedirection
        ld (hl),&df
        inc hl
        ld (hl),c
        inc hl
        ld (hl),b
        inc hl
        dec a
        jp nz,loopinstallredirection

        ret

;
; Sauve les indirections de gestion cassette si elle n'ont pas �t� d�j� prises en charge
;
; Entr�e - IY = Adresse de la zone m�moire de la ROM
; Sortie - N/A
; Alt�r� - AF,BC,DE,HL
;

SaveTapeJumpBlock
        GET_A amsdosrom
        or a
        ret nz
        GETPTR_DE tapejumpblocksaved
        ld hl,_cas_in_open              ; HL = Premi�re indirection
        ld bc,13*3
        ldir
        ret

;
; Redirige l'appel "far fall" vers le bon vecteur
; et le corrige l'adresse de retour
;

CAS_VectorDispatcher
        ; Attention ici, on peut �tre invoqu� alors que les ROMs ne sont
        ; pas initialis�es (notamment suite � un "RUN" du firmware),
        ; il faut r�cup�rer � la main l'adresse de notre IY...
        ; L'Amsdos va le r�cup�rer � une adresse statique, on fait pareil !
        ld iy,(_dos_rom_base_address)

        di
        ex af,af'
        exx
        ld a,c
        pop de
        pop bc
        pop hl
        ex (sp),hl
        push bc
        push de
        ld c,a
        ld b,&7f
        ld de,cas_jumptable-_cas_in_open-3
        add hl,de
        push hl
        exx
        ex af,af'
        ei
        ret

Firm_PlusMenu
        ld hl,0
        rst &28:dw &0077    ; rst 5,&0077
        ret

CAS_JumpTable
        jp cas_inopen
        jp cas_inclose
        jp cas_inabandon
        jp cas_inchar
        jp cas_indirect
        jp cas_return
        jp cas_testeof
        jp cas_outopen
        jp cas_outclose
        jp cas_outabandon
        jp cas_outchar
        jp cas_outdirect
        jp cas_catalog

        Read"CAS_InVectors.a"
        Read"CAS_OutVectors.a"
        Read"CAS_Catalog.a"
        Read"CAS_Misc.a"

        Read"BIOS.a"
        Read"RSX.a"

        Read"Messages.a"
        Read"Display.a"
        Read"Compute.a"
        Read"LowLevel.a"
        Read"Misc.a"
        Read"DirScan.a"
        Read"Devices.a"
        Read"ConfigFile.a"
        Read"DataAccess.a"
        Read"AlienDOS.a"

CodeEnd

