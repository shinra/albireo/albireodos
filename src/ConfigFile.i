;
; Flags de paramétrage pour la sauvegarde et le chargement du fichier de configuration
;

; Bits
SC_IndirectionB         Equ 0
SC_CurrentLogicalDriveB Equ 1
SC_CurrentPathsB        Equ 2
SC_CurrentFilesCommonB  Equ 3
SC_CurrentFilesMoreB    Equ 4
SC_ReadBufferB          Equ 5

; Flags
SC_IndirectionF         Equ 1 << sc_indirectionb
SC_CurrentLogicalDriveF Equ 1 << sc_currentlogicaldriveb
SC_CurrentPathsF        Equ 1 << sc_currentpathsb
SC_CurrentFilesCommonF  Equ 1 << sc_currentfilescommonb
SC_CurrentFilesMoreF    Equ 1 << sc_currentfilesmoreb
SC_ReadBufferF          Equ 1 << sc_readbufferb

; Masks
SC_CurrentFilesM        Equ sc_currentfilescommonf + sc_currentfilesmoref

