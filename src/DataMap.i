;
; Donn�es d'ex�cution (relatif � IY)
; L'organisation est cens�e �tre la plus compatible possible avec l'Amsdos
; Les * indiquent des donn�es sp�cifiques � l'AlbiDOS
;

Struct FileHead

    User        ds 1
    Name        ds 8
    Ext         ds 3
                ds 6
    Type        ds 1    ; Voir DSK_FILE_TYPE_*
    BufferLeft  ds 2    ; Nombre d'octets �crits/lus dans le tampon de travail (donn�e temporaire)
    LoadAdr     ds 2    ; Adresse de chargement (ou adresse du tampon pour un fichier ASCII)
                ds 1
    Length      ds 2    ; Longueur du fichier (&0000 pour un fichier ASCII)
    Entry       ds 2    ; Adresse d'ex�cution
                ds 36
    Length24b   ds 3    ; Longueur du fichier sur 24 bits
    CSH         ds 2    ; Check Sum of Header

EndStruct

FileHeadSize Equ {sizeof}FileHead


Struct ChunkHead

    ID          ds 2    ; Identifiant du chunk
    Size        ds 2    ; Taille des donn�es du chunk (header non compris)

EndStruct

ChunkHeadSize Equ {sizeof}ChunkHead

MaxLinkSize   Equ 52


; Identifiants des lecteurs physiques administr�s par AlbiDOS (sous la forme %00000nnn)
; L'ajout dynamique par d'autres ROM sera possible � l'avenir (sous la format %rrrrrnnn o� r est le num�ro de la ROM et n du lecteur)
PhysicalDrive_DFA   Equ &00 ; Via l'AMSDOS (&00 = lecteur A)
PhysicalDrive_DFB   Equ &01 ; Via l'AMSDOS (&01 = lecteur B)
PhysicalDrive_SD    Equ &02 ; En direct
PhysicalDrive_UMS   Equ &03 ; En direct
PhysicalDrive_TAPE  Equ &07 ; Via le firmware
; Valeurs par d&faut pour les lecteurs logiques A et B
DefaultPhysicalDrive_A  Equ physicaldrive_sd
DefaultPhysicalDrive_B  Equ physicaldrive_ums

DirMaxDepth   Equ 6
DirBufferSize Equ 1+DirMaxDepth*(8+3)


; Pour OptionFlags (r�gl�es via le vecteur BIOS CTRL-J)
; Bits
OPT_CatFakedParentB Equ 0
OPT_DisabledLinkB   Equ 1
OPT_EmulateSoftEOFB Equ 2
; Flags
OPT_CatFakedParentF Equ 1 << opt_catfakedparentb
OPT_DisabledLinkF   Equ 1 << opt_disabledlinkb
OPT_EmulateSoftEOFF Equ 1 << opt_emulatesofteofb


; Valeur possible pour LowLevelFile
LLF_KindUnknown         Equ 0
LLF_KindInStream        Equ 1
LLF_KindOutStream       Equ 2
LLF_KindExamineDir      Equ 3
LLF_KindNone            Equ -1


Struct DataMap

    CurrentLogicalDrive     ds 1                ; Lecteur logique courant (0=A, 1=B) - Pour l'Amsdos lecteur logique = lecteur physique
    CurrentUser             ds 1                ; User courant (mis � 0 � l'init de l'AlbiDOS mais inutilis�)

                            ds 6                ; R�serv� Amsdos

_Start_CommonFileHandle
    FileInDrive             ds 1                ; &FF si le fichier d'entr�e est ferm�, lecteur sur lequel il est ouvert sinon
                            ds 1                ; R�serv� Amsdos
    FileInName              ds 8                ; Nom du fichier d'entr�e
    FileInExt               ds 3                ; Extension du fichier d'entr�e
                            ds 23               ; R�serv� Amsdos

    FileOutDrive            ds 1                ; &FF si le fichier de sortie est ferme, lecteur sur lesquel il est ouvert sinon
                            ds 1                ; R�serv� Amsdos
    FileOutName             ds 8                ; Nom du fichier de sortie
    FileOutExt              ds 3                ; Extension du fichier de sortie
                            ds 23               ; R�serv� Amsdos

    FileInMode              ds 1                ; Voir DSK_FILE_MODE_*
    FileInBufferBase        ds 2                ; Adresse de base du tampon de 2K pour la lecture octet par octet
    FileInBufferCur         ds 2                ; Adresse courante du tampon

    struct FileHead         FileInHead          ; Zone de header du fichier d'entr�e

    FileOutMode             ds 1                ; Voir DSK_FILE_MODE_*
    FileOutBufferBase       ds 2                ; Adresse de base du tampon de 2K pour l'�criture octet par octet
    FileOutBufferCur        ds 2                ; Adresse courante du tampon

    Struct FileHead         FileOutHead         ; Zone de header du fichier de sortie
_End_CommonFileHandle

    WorkArea                ds 128              ; Zone de stockage des enregistrements et de formatage des noms de fichier

    TapeJumpBlockSaved      ds 13*3
    AmsdosFarAddress        ds 3                ; Donn�es du "far address" pour les indirections des vecteurs entr�es/sorties

    AmsdosFree              ds 2                ; Zone non utilis�e par l'Amsdos

                            ds 25*2             ; R�serv� Amsdos
                            ds 16               ; R�serv� Amsdos
                            ds 94               ; R�serv� Amsdos

    ; Le buffer de chargement des secteurs de l'AMSDOS nous est inutile en tant que tel
    ; On y stocke donc nos donn�es de travail volatiles ci-dessous (utilis�es uniquement le temps d'une routine de haut niveau CAS ou RSX)

    DirectoryBuffer                             ; Zone de stockage des entr�es de r�pertoire

    WorkDrive               ds 1                ; * Zone de stockage du lecteur physique pendant le traitement
    WorkPath                ds dirbuffersize    ; * Zone de stockage du chemin pendant le traitement
    WorkLogicalDrive        ds 1                ; * Zone de stockage du lecteur logique pendant l'analyse des chemins ('A', 'B' ou &00)

    TempBufferBase          ds 2                ; * Zone de stockage temporaire de l'adresse du tampon d'entr�e/sortie

    CatalogOverflow         ds 1                ; * &FF Si le catalog ne tient pas dans 2K
    FileIsDir               ds 1                ; * Variable pour savoir si le nom en entr�e est explicitement un r�pertoire (chemin termin� par /)
    MediaStatus             ds 1                ; * Si &FF le fichier est ouvert sur un media de type flux
    RootReached             ds 1                ; * Flag de tentative de d�passement de la racine d'un disque

    FileInDefaultExtCheck   ds 2                ; * Gestion des extensions par defaut (BAS/BIN/...) � l'ouverture d'un fichier
    FileIn1stRecordSize     ds 2                ; * Taille effective du premier enregistrement lu pour le d�codage de l'ent�te Amsdos
    FileOutByteToWrite      ds 1                ; * Variable temporaire avec l'octet en cours d'ajout dans le tampon d'�criture
    FileOutExtFound         ds 1                ; * Flag de pr�sence de l'extension dans le fichier � ouvrir

    DirCount                ds 2                ; * Nombre d'entr�es trait�es pendant le DirScan
    EntryFound              ds 1                ; * &FF si le DirScan a trouv� au moins un fichier au cours de la session
    SetProtection           ds 2                ; * Protections de fichier en cours de r�glage pour un DirScan/Hide-Show-Protect-Unprotect
    EraAskMask              ds 1                ; * Masque du choix utilisateurs � la demande de confirmation pendant un DirScan/Era
    EraSkipCount            ds 2                ; * Nombre d'entr�es saut�es pendant un DirScan/Era

    DriveName               ds 8                ; * Nom du lecteur pendant la cr�ation d'un message DOS

    Unused1                 ds 128-dirbuffersize-30

    ; Le buffer de chargement des secteurs de l'AMSDOS nous est inutile en tant que tel
    ; On y stocke donc �galement nos donn�es de travail persistantes et semi-persistantes

    ReadBuffer              ds 128              ; D�but du buffer de lecture (utilis� pour la lecture ASCII et le DirScan)

    ; Donn�es semi-persistances li�es � la gestion des sessions d'ouvertures de fichiers/r�pertoires

_Start_MoreFileHandle
    ; Zone d�di�e � la gestion du flux en entr�e (priv� au noeud DOS sur lequel le flux est ouvert)
    FileInPath              ds dirbuffersize    ; * Albireo DevNode: R�pertoire du fichier en lecture
    FileInOffset            ds 4                ; * Albireo DevNode: Position globale dans le fichier qui correspond au tampon en cours
    Padding1                ds 80-dirbuffersize-4
    ; Zone d�di�e � la gestion du flux en sortie (priv� au noeud DOS sur lequel le flux est ouvert)
    FileOutPath             ds dirbuffersize    ; * Albireo DevNode: R�pertoire du fichier en �criture
    FileOutOffset           ds 4                ; * Albireo DevNode: Position globale dans le fichier qui correspond au tampon en cours
    FileOutCreated          ds 1                ; * Albireo DevNode: Flag qui permet de savoir si le fichier en cours a d�j� �t� cr�� ou non
    Padding2                ds 80-dirbuffersize-5
    ; Zone d�di�e au parcours des r�pertoires (priv� au noeud DOS sur lequel le parcours est en lanc�)
    ExamineDirPath          ds dirbuffersize    ; * Albireo DevNode: R�pertoire du Examine sur un r�pertoire en cours
    ExamineDirPos           ds 2                ; * Albireo DevNode: Position dans le examine sur un r�pertoire en cours
    Padding3                ds 80-dirbuffersize-2
    ; Donn�es priv�es de l'AlbiDOS
    ExamineDirDrive         ds 1                ; * &FF si aucun Examine sur un r�pertoire n'est en cours, lecteur sur lequel il est en cours sinon
    FileInByteReturned      ds 1                ; * CAS indirections: 0 si aucun octet n'a �t� retourne dans le tampon
    FileInLastReadByte      ds 1                ; * Variable temporaire avec l'octet en cours de lecture dans le tampon de lecture
_End_MoreFileHandle

    ; Donn�es persistantes avec l'�tat courant des chemins et lecteurs physiques associ�s aux lecteurs logiques

_Start_CurrentPaths
    CurrentPhysicalDriveA   ds 1                ; * AlbiDOS: Lecteur physique courant sur le lecteur logique A
    CurrentPathA            ds dirbuffersize    ; * AlbiDOS: R�pertoire courant sur le lecteur logique A
    CurrentPhysicalDriveB   ds 1                ; * AlbiDOS: Lecteur physique courant sur le lecteur logique B
    CurrentPathB            ds dirbuffersize    ; * AlbiDOS: R�pertoire courant sur le lecteur logique B
_End_CurrentPaths

    Unused2                 ds 512-128-3*80-2*dirbuffersize-5

    ; Les donn�es de la fin de la zone m�moire de la ROM ne sont jamais touch�es par l'AMSDOS,
    ; on peut les utiliser pour y stocker des informations persistantes importantes (jusqu'� 80 octets)

    TxtOutputFarAddress     ds 3                ; * TXT OUPUT hook: donn�es du "far address"
    TxtOutputJump           ds 3                ; * TXT OUPUT hook: saut originel
    TxtOutputPos            ds 1                ; * TXT OUPUT hook: position courante dans la cha�ne � modifier
    Struct ChunkHead        CfgChunkHead        ; * Config: donn�es du chunk en cours de traitement
    ChunkIndirection        ds 1                ; * Config: �tat d'activation des indirections
    ChunkSeek               ds 4                ; * Config: position courante dans le fichier
    LinkPath                ds maxlinksize      ; * Lien: zone de gestion temporaire
    LowLevelFile            ds 1                ; * Flag interne qui permet de savoir quel est fichier courant ouvert sur le ch376 de l'Albireo (lowlevel)
    LowLevelMount           ds 1                ; * Flag interne qui permet de savoir quel est le p�riph�rique mount� sur le ch376 de l'Albireo (lowlevel)
    OptionFlags             ds 1                ; * Options: flag de configuration non persistants, voir OPT_*
    AlbireoRevision         ds 1                ; * Init: r�vision de la carte Albireo trouv�e
    AmsdosROM               ds 1                ; * Init: ROM sous-jacente � utiliser pour passer la main � l'AMSDOS lorsque l'AlbiDOS est inactif
    FarAddress              ds 3                ; * Init: donn�es du "far address" pour les indirections des vecteurs entr�es/sorties
    ROMTag                  ds 4                ; * Init: tag de ROM pour le cha�nage syst�me manuel

EndStruct

DataSize Equ {sizeof}DataMap

;
; Macros de r�cup�ration des pointeurs vers les donn�es (relatives � IY)
;

Macro GETPTR_HL offset
        push bc
        ld bc,datamap.{offset}
        call getpointerhl
        pop bc
MEnd

Macro GETPTR_DE offset
        ex de,hl
        GETPTR_HL {offset}
        ex de,hl
MEnd

Macro GETPTR_IX offset
        push bc
        ld bc,datamap.{offset}
        call getpointerix
        pop bc
MEnd

;
; Macros g�n�riques d'acc�s aux donn�es (relatives � IY)
;
; Note : � ne pas utiliser directement, utiliser plut�t
;        les macros d�di�es qui sont d�clar�es apr�s
;

Macro GET_R8 r8,offset,aux
        if datamap.{offset}>127
        push {aux}
        ld {aux},datamap.{offset}
        call getr8_{r8}
        pop {aux}
        else
        ld {r8},(iy+datamap.{offset})
        endif
MEnd

Macro PUT_R8 offset,r8,aux
        if datamap.{offset}>127
        push {aux}
        ld {aux},datamap.{offset}
        call putr8_{r8}
        pop {aux}
        else
        ld (iy+datamap.{offset}),{r8}
        endif
MEnd

Macro GET_R16 r16h,r16l,offset
        if datamap.{offset}>126
        ld {r16h}{r16l},datamap.{offset}
        call getr16_{r16h}{r16l}
        else
        ld {r16l},(iy+datamap.{offset}+0)
        ld {r16h},(iy+datamap.{offset}+1)
        endif
MEnd

Macro PUT_R16 offset,r16h,r16l,aux
        if datamap.{offset}>126
        push {aux}
        ld {aux},datamap.{offset}
        call putr16_{r16h}{r16l}
        pop {aux}
        else
        ld (iy+datamap.{offset}+0),{r16l}
        ld (iy+datamap.{offset}+1),{r16h}
        endif
MEnd

Macro GET_R24 r24h,r24i,r24l,offset
        if datamap.{offset}>125
        ld {r24i}{r24l},datamap.{offset}
        call getr24_{r24h}{r24i}{r24l}
        else
        ld {r24l},(iy+datamap.{offset}+0)
        ld {r24i},(iy+datamap.{offset}+1)
        ld {r24h},(iy+datamap.{offset}+2)
        endif
MEnd

Macro PUT_R24 offset,r24h,r24i,r24l,aux
        if datamap.{offset}>125
        push {aux}
        ld {aux},datamap.{offset}
        call putr24_{r24h}{r24i}{r24l}
        pop {aux}
        else
        ld (iy+datamap.{offset}+0),{r24l}
        ld (iy+datamap.{offset}+1),{r24i}
        ld (iy+datamap.{offset}+2),{r24h}
        endif
MEnd

Macro GET_R32 r32hh,r32hl,r32lh,r32ll,offset
        if datamap.{offset}>124
        push iy
        push bc
        ld bc,datamap.{offset}
        add iy,bc
        pop bc
        ld {r32ll},(iy+0)
        ld {r32lh},(iy+1)
        ld {r32hl},(iy+2)
        ld {r32hh},(iy+3)
        pop iy
        else
        ld {r32ll},(iy+datamap.{offset}+0)
        ld {r32lh},(iy+datamap.{offset}+1)
        ld {r32hl},(iy+datamap.{offset}+2)
        ld {r32hh},(iy+datamap.{offset}+3)
        endif
MEnd

Macro PUT_R32 offset,r32hh,r32hl,r32lh,r32ll
        if datamap.{offset}>124
        push iy
        push bc
        ld bc,datamap.{offset}
        add iy,bc
        pop bc
        ld (iy+0),{r32ll}
        ld (iy+1),{r32lh}
        ld (iy+2),{r32hl}
        ld (iy+3),{r32hh}
        pop iy
        else
        ld (iy+datamap.{offset}+0),{r32ll}
        ld (iy+datamap.{offset}+1),{r32lh}
        ld (iy+datamap.{offset}+2),{r32hl}
        ld (iy+datamap.{offset}+3),{r32hh}
        endif
MEnd

;
; Macros d'acc�s aux donn�es par registres (relatives � IY)
;

Macro GET_A offset
        GET_R8 a,{offset},bc
MEnd

Macro GET_B offset
        GET_R8 b,{offset},de
MEnd

Macro GET_C offset
        GET_R8 c,{offset},de
MEnd

Macro GET_D offset
        GET_R8 d,{offset},bc
MEnd

Macro GET_E offset
        GET_R8 e,{offset},bc
MEnd

Macro GET_H offset
        GET_R8 h,{offset},bc
MEnd

Macro GET_L offset
        GET_R8 l,{offset},bc
MEnd

Macro GET_BC offset
        GET_R16 b,c,{offset}
MEnd

Macro GET_DE offset
        GET_R16 d,e,{offset}
MEnd

Macro GET_HL offset
        GET_R16 h,l,{offset}
MEnd

Macro GET_CDE offset
        GET_R24 c,d,e,{offset}
MEnd

Macro GET_BCDE offset
        GET_R32 b,c,d,e,{offset}
MEnd

Macro GET_BCHL offset
        GET_R32 b,c,h,l,{offset}
MEnd

Macro GET_DEHL offset
        GET_R32 d,e,h,l,{offset}
MEnd

Macro PUT_A offset
        PUT_R8 {offset},a,bc
MEnd

Macro PUT_B offset
        PUT_R8 {offset},b,de
MEnd

Macro PUT_C offset
        PUT_R8 {offset},c,de
MEnd

Macro PUT_D offset
        PUT_R8 {offset},d,bc
MEnd

Macro PUT_E offset
        PUT_R8 {offset},e,bc
MEnd

Macro PUT_H offset
        PUT_R8 {offset},h,bc
MEnd

Macro PUT_L offset
        PUT_R8 {offset},l,bc
MEnd

Macro PUT_BC offset
        PUT_R16 {offset},b,c,de
MEnd

Macro PUT_DE offset
        PUT_R16 {offset},d,e,bc
MEnd

Macro PUT_HL offset
        PUT_R16 {offset},h,l,bc
MEnd

Macro PUT_CDE offset
        PUT_R24 {offset},c,d,e,hl
MEnd

Macro PUT_BCDE offset
        PUT_R32 {offset},b,c,d,e
MEnd

Macro PUT_BCHL offset
        PUT_R32 {offset},b,c,h,l
MEnd

Macro PUT_DEHL offset
        PUT_R32 {offset},d,e,h,l
MEnd

;
; Macros de modification par valeur des donn�es (relatives � IY)
;

Macro PUT_8 offset,val
        if datamap.{offset}>127
        push af
        ld a,{val}
        PUT_A {offset}
        pop af
        else
        PUT_R8 {offset},{val},bc
        endif
MEnd

Macro PUT_16 offset,val
        if datamap.{offset}>127
        push de
        ld de,{val}
        PUT_DE {offset}
        pop de
        else
        PUT_R16 {offset},{val} >> 8,{val} and 255,bc
        endif
MEnd

Macro PUT_24 offset,val
        push bc
        push de
        ld c,{val} >> 16
        ld de,{val} and &ffff
        PUT_R24 {offset},c,d,e,hl
        pop de
        pop bc
MEnd

Macro PUT_32 offset,val
        PUT_R32 {offset},(({val} >> 24) and &ff),(({val} >> 16) and &ff),(({val} >> 8) and &ff),({val} and &ff),
MEnd

Macro INC_8 offset
        if datamap.{offset}>127
        push bc
        call inc8
        ld bc,datamap.{offset}
        pop bc
        else
        inc (iy+datamap.{offset})
        endif
MEnd

Macro INC_16 offset
        if datamap.{offset}>126
        push bc
        ld bc,datamap.{offset}
        call inc16
        pop bc
        else
        push bc
        ld c,(iy+datamap.{offset}+0)
        ld b,(iy+datamap.{offset}+1)
        inc bc
        ld (iy+datamap.{offset}+0),c
        ld (iy+datamap.{offset}+1),b
        pop bc
        endif
MEnd

Macro DEC_8 offset
        if datamap.{offset}>127
        push bc
        ld bc,datamap.{offset}
        call dec8
        pop bc
        else
        dec (iy+datamap.{offset})
        endif
MEnd

Macro DEC_16 offset
        if datamap.{offset}>126
        push bc
        ld bc,datamap.{offset}
        call dec16
        pop bc
        else
        push bc
        ld c,(iy+datamap.{offset}+0)
        ld b,(iy+datamap.{offset}+1)
        dec bc
        ld (iy+datamap.{offset}+0),c
        ld (iy+datamap.{offset}+1),b
        pop bc
        endif
MEnd

;
; Macros de comparaison des donn�es (relatives � IY)
;

Macro CP_A offset
        if datamap.{offset}>127
        push iy
        push bc
        ld bc,datamap.{offset}
        add iy,bc
        pop bc
        cp (iy)
        pop iy
        else
        cp (iy+datamap.{offset})
        endif
MEnd

