;
; RAM
;

Macro RAM_LAM
    	rst &20
MEnd

;
; RST
;

Macro FAR_CALL vector
        rst &18
        dw {vector}
MEnd

If 0
Macro FAR_PCHL
        call &1b
MEnd

Macro BRK
        rst &30
MEnd
EndIf

;
; Kernel
;

_KL_U_ROM_DISABLE       Equ &b903
_KL_L_ROM_DISABLE       Equ &b909
_KL_ROM_RESTORE         Equ &b90c
_KL_ROM_SELECT          Equ &b90f
_KL_CURR_SELECTION      Equ &b912
_KL_PROBE_ROM           Equ &b915
_KL_LDIR                Equ &b91b
_KL_LDDR                Equ &b91e
_KL_INIT_BACK           Equ &bcce
_KL_FIND_COMMAND        Equ &bcd4

;
; Key manager
;

_KM_WAIT_CHAR           Equ &bb06
_KM_WAIT_KEY            Equ &bb18
_KM_READ_KEY            Equ &bb1b

;
; Text VDU
;

_TXT_VDU_ENABLE         Equ &bb54
_TXT_VDU_DISABLE        Equ &bb57
_TXT_OUTPUT             Equ &bb5a
_TXT_RD_CHAR            Equ &bb60
_TXT_SET_CURSOR         Equ &bb75
_TXT_GET_CURSOR         Equ &bb78
_TXT_CUR_ON             Equ &bb81
_TXT_CUR_OFF            Equ &bb84
_TXT_SET_PEN            Equ &bb90
_TXT_GET_PEN            Equ &bb93

;
; Screen pack
;

_SCR_GET_MODE           Equ &bc11

;
; Cassette
;

_CAS_IN_OPEN            Equ &bc77
_CAS_IN_CLOSE           Equ &bc7a
_CAS_IN_ABANDON         Equ &bc7d
_CAS_IN_CHAR            Equ &bc80
_CAS_IN_DIRECT          Equ &bc83
_CAS_RETURN             Equ &bc86
_CAS_TEST_EOF           Equ &bc89
_CAS_OUT_OPEN           Equ &bc8c
_CAS_OUT_CLOSE          Equ &bc8f
_CAS_OUT_ABANDON        Equ &bc92
_CAS_OUT_CHAR           Equ &bc95
_CAS_OUT_DIRECT         Equ &bc98
_CAS_CATALOG            Equ &bc9b

;
; Machine pack
;

_MC_WAIT_FLYBACK        Equ &bd19

;
; Noeuds DOS (AlbiDOS)
;

_DOSNODE_INIT                       Equ &c009
_DOSNODE_CHECK_DRIVE                Equ  1*3+_dosnode_init
_DOSNODE_GET_STATUS                 Equ  2*3+_dosnode_init
_DOSNODE_GET_NAME                   Equ  3*3+_dosnode_init
_DOSNODE_GET_DESC                   Equ  4*3+_dosnode_init
_DOSNODE_GET_FREE_SPACE             Equ  5*3+_dosnode_init
_DOSNODE_IN_OPEN_STREAM             Equ  6*3+_dosnode_init
_DOSNODE_IN_READ_STREAM             Equ  7*3+_dosnode_init
_DOSNODE_IN_CLOSE_STREAM            Equ  8*3+_dosnode_init
_DOSNODE_IN_SEEK_STREAM             Equ  9*3+_dosnode_init
_DOSNODE_OUT_OPEN_STREAM            Equ 10*3+_dosnode_init
_DOSNODE_OUT_WRITE_STREAM           Equ 11*3+_dosnode_init
_DOSNODE_OUT_CLOSE_STREAM           Equ 12*3+_dosnode_init
_DOSNODE_OUT_SEEK_STREAM            Equ 13*3+_dosnode_init
_DOSNODE_EXAMINE                    Equ 14*3+_dosnode_init
_DOSNODE_EXAMINE_NEXT               Equ 15*3+_dosnode_init
_DOSNODE_RENAME                     Equ 16*3+_dosnode_init
_DOSNODE_DELETE                     Equ 17*3+_dosnode_init
_DOSNODE_CREATE_DIR                 Equ 18*3+_dosnode_init
_DOSNODE_SET_PROTECTION             Equ 19*3+_dosnode_init
_DOSNODE_RESERVED_1                 Equ 20*3+_dosnode_init
_DOSNODE_RESERVED_2                 Equ 21*3+_dosnode_init
_DOSNODE_RESERVED_3                 Equ 22*3+_dosnode_init
_DOSNODE_RESERVED_4                 Equ 23*3+_dosnode_init
_DOSNODE_RESERVED_5                 Equ 24*3+_dosnode_init
_DOSNODE_RESERVED_6                 Equ 25*3+_dosnode_init
_DOSNODE_RESERVED_7                 Equ 26*3+_dosnode_init
_DOSNODE_RESERVED_8                 Equ 27*3+_dosnode_init
_DOSNODE_RESERVED_9                 Equ 28*3+_dosnode_init
_DOSNODE_RESERVED_10                Equ 29*3+_dosnode_init
_DOSNODE_RESERVED_11                Equ 30*3+_dosnode_init
_DOSNODE_RESERVED_12                Equ 31*3+_dosnode_init

