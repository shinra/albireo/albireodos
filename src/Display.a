;
; Fonctions d'affichage
;

;
; Affichage d'une chaine de caract�res
; La version "Err" affiche uniquement si les messages sont activ�s
; La version "AltPen" affiche en utilisant la couleur alternative
;
; Entr�e - HL = pointeur vers la cha�ne de caract�res (termin�e par un bit 7 � 1)
; Sortie - HL = pointeur vers le caract�re suivant apr�s la fin de la cha�ne
; Alt�r� - AF,HL

PrintErrString
        ld a,(dsk_message_status)
        cp &ff
        ret z
PrintString
        ld a,(hl)
        rla
        jr c,printstringend
        rra
        call _txt_output
        inc hl
        jr printstring
PrintStringEnd
        srl a
        jp _txt_output
PrintAltPenString
        call setalternatepen
        push af
        call printstring
        pop af
        call _txt_set_pen
        ret
;
; Recopie une cha�ne de caract�res
;
; Entr�e - HL = cha�ne de caract�re � copier (termin�e par un bit 7 � 1)
;          DE = destination de la copie
;           C = nombre maximum de caract�res � copier
; Sortie - Si C alors la copie s'est bien termin�e
;            HL pointe sur le premier caract�re apr�s la fin de la cha�ne
;            DE pointe sur le premier caract�re apr�s la fin de la copie de la cha�ne
;             C a �t� d�cr�ment� du nombre de caract�res copi�s
;          Si NC alors la copie n'a pas pu se faire (nombre maximum de caract�res atteint)
;             C vaut 0, HL et DE sont ind�termin�s
; Alt�r� - AF,C,DE,HL
;

CopyString
        ld a,c
        or a
        ret z   ; NC
        ld a,(hl)
        ldi
        rla
        jr nc,copystring
        ret     ; C

;
; Affichage avec justification � droite d'une chaine de caract�res
;
; Entr�e - HL = pointeur vers la cha�ne de caract�res (termin�e par un bit 7 � 1)
;           A = position de la marge de droite
; Sortie - HL = pointeur vers le caract�re suivant apr�s la fin de la cha�ne
; Alt�r� - AF,HL

PrintJustifiedString
        push bc
        call printjustifiedstringinternal
        pop bc
        ret
PrintJustifiedStringInternal
        call getstringlen
        sub a,c
        jr z,printstring
        ld b,a
        ld a,' '
PJSLoop
        call _txt_output
        djnz pjsloop
        jr printstring

;
; Calcul de la taille d'une cha�ne de caract�res
;
; Entr�e - HL = pointeur vers la cha�ne de caract�res (termin�e par un bit 7 � 1)
; Sortie - C =  taille de la cha�ne
; Alt�r� - C

GetStringLen
        push hl
        call getstringleninternal
        pop hl
        ret
GetStringLenInternal
        ld c,0
GSLLoop inc c
        bit 7,(hl)
        ret nz
        inc hl
        jr gslloop

;
; Affichage d'une cha�ne de caract�res de taille donn�e
;
; Entr�e - HL = pointeur vers la cha�ne de caract�res
;           B = longueur de la cha�ne � afficher
; Sortie - HL = pointeur sur le prochain caract�re � afficher
;           B = 0
; Alt�r� - AF,BC,HL

PrintSizedString
        ld a,(hl)
        inc hl
        call _txt_output
        djnz printsizedstring
        ret

;
; Affiche un nom de fichier normalis�
; La version "Err" affiche uniquement si les messages sont activ�s
; Note: le nom affich� est pr�c�d� d'un retour � la ligne comme l'AMSDOS
;       sauf pour le version "Raw"
;
; Entr�e - HL = adresse du nom de fichier normalis�
; Sortie - Aucune
; Alt�r� - AF,BC,HL

PrintErrNormalizedFileName
        ld a,(dsk_message_status)
        cp &ff
        ret z
PrintNormalizedFileName
        push hl
        ld hl,msg_newline
        call printstring
        pop hl
PrintRawNormalizedFileName
        ld b,8
        call printsizedstring
        call printdot
        ld b,3
InOpenNotFoundExtLoop
        ld a,(hl):and &7f:inc hl:call _txt_output
        djnz inopennotfoundextloop
        ret

;
; Affichage d'une taille en Ko/Mo/Go/To
;
; Entr�e - BCDE = taille en kilo-octets
; Sortie - Rien
; Alt�r� - AF,BC,DE,HL

PrintSmartSize
        ld a,%11000000
        and b
        jp nz,printsmartsizeto
        ld a,%00111111
        and b
        jp nz,printsmartsizego
        ld a,%11110000
        and c
        jp nz,printsmartsizego
        ld a,%00001111
        and c
        jp nz,printsmartsizemo
        ld a,%11111100
        and d
        jp nz,printsmartsizemo
PrintSmartSizeKo
        call printnumber
        ld a,'K':jp _txt_output
PrintSmartSizeMo
        srl c:rr d:rr e
        srl c:rr d:rr e
        ld e,d
        ld d,c
        call printnumber
        ld a,'M':jp _txt_output
PrintSmartSizeGo
        srl b:rr c
        srl b:rr c
        srl b:rr c
        srl b:rr c
        ld e,c
        ld d,b
        call printnumber
        ld a,'G':jp _txt_output
PrintSmartSizeTo
        srl b
        srl b
        srl b
        srl b
        srl b
        srl b
        ld e,b
        ld d,0
        call printnumber
        ld a,'T':jp _txt_output

;
; Affichage d'un nombre sur 4 digits
;
; Entr�e - DE = nombre
; Sortie - Rien
; Alt�r� - AF,BC,DE,HL

PrintNumber
        ld b,0 ; Flag pour savoir si on doit mettre un z�ro ou un espace
        ld a,d
        ld c,e
        ld de,1000
        push bc
        call div
        ld a,c
        pop bc
        call printdigit
        ld a,h
        ld c,l
        ld de,100
        push bc
        call div
        ld a,c
        pop bc
        call printdigit
        ld a,h
        ld c,l
        ld de,10
        push bc
        call div
        ld a,c
        pop bc
        call printdigit
        ld b,-1 ; On affiche toujours le 0 des unit�s
        ld a,l
        call printdigit
        ret
PrintDigit
        cp b    ; Tant qu'on a encore rien affich�, on met des espaces � la place des z�ros
        jp z,printspace
        ld b,-1 ; � partir de maintenant on mettra toujours les z�ros
        add a,'0'
        jp _txt_output

;
; Affiche un nombre hexad�cimal 16 bits
;
; Entr�e - BC = nombre � afficher
; Sortie - N/A
; Alt�r� - AF

PrintHexaNumber
        push af
        ld a,b
        call printhexanumber8
        ld a,c
        call printhexanumber8
        pop af
        ret
;
; Affiche un nombre hexad�cimal 8 bits
;
; Entr�e - A = nombre � afficher
; Sortie - N/A
; Alt�r� - AF

PrintHexaNumber8
        push af
        push hl
        push bc
        push af
        srl a
        srl a
        srl a
        srl a
        ld c,a
        ld b,0
        ld hl,hexachar
        add hl,bc
        ld a,(hl)
        call _txt_output
        pop af
        and &f
        ld c,a
        ld hl,hexachar
        add hl,bc
        ld a,(hl)
        call _txt_output
        pop bc
        pop hl
        pop af
        ret
HexaChar
        db "0123456789ABCDEF"

;
; Affiche un message associ� � un lecteur physique sous la forme
; <drive name>: message
;
; Entr�e - A = lecteur physique
;          HL = message
; Sortie - Aucune
; Alt�r� - AF,HL

DisplayDriveMsg
        push de
        push hl
        ld hl,msg_drive
        push af
        call printstring
        pop af
        GETPTR_HL drivename
        ex de,hl
        push de
        call device_getname
        call nc,storeunknownstring
        pop hl
        call printstring
        ld hl,msg_colon
        call printstring
        pop hl
        call printstring
        ld hl,msg_newline
        call printstring
        pop de
        ret

;
; Affiche l'invite Retry, Ignore or Cancel?
;
; Entr�e - A = lecteur logique
;          HL = message
; Sortie - A = 'R', 'I' ou 'C'
; Alt�r� - AF,HL

AskRetry
        call displaydrivemsg
        ld hl,msg_retryignoreorcancel
        call printstring
        call _txt_cur_on                 ; Curseur syst�me actif
AskWait call _km_wait_char
        call toupper
        cp 'R':jp z,askok
        cp 'I':jp z,askok
        cp 'C':jp z,askok
        jp askwait
AskOk   call _txt_output
        call _txt_cur_off                ; Curseur syst�me inactif
        ld hl,msg_newline
        push af
        call printstring
        pop af
        ret

;
; Affiche l'invite Confirm? Yes, No, All?
;
; Entr�e - N/A
; Sortie - A = 'Y', 'N' ou 'A'
; Alt�r� - AF,HL

Confirm ld hl,msg_confirmyesnoorall
        call printstring
        call _txt_cur_on                 ; Curseur syst�me actif
ConfirmWait
        call _km_wait_char
        call toupper
        cp 'Y':jp z,confirmok
        cp 'N':jp z,confirmok
        cp 'A':jp z,confirmok
        jp confirmwait
ConfirmOk
        call _txt_output
        call _txt_cur_off                ; Curseur syst�me inactif
        ld hl,msg_newline
        push af
        call printstring
        pop af
        ret

;
; Positionne le pen alternatif
;
; Entr�e - N/A
; Sortie - A = pen pr�c�demment actif
; Alt�r� - AF

SetAlternatePen
        push hl
        call _txt_get_pen
        push af

        ld a,2
        call _txt_set_pen    ; pen 2 si mode 0 ou 1
        call _scr_get_mode
        cp 2
        ld a,1
        call z,_txt_set_pen  ; pen 1 si mode 2

        pop af
        pop hl
        ret

;
; Routines d'affichages diverses
;

PrintDot
        ld a,'.'
        jp _txt_output
PrintDoubleSpace
        ld a,' '
        call _txt_output
PrintSpace
        ld a,' '
        jp _txt_output

;
; Installe l'indirection de modification de la sortie Amsdos
;
; Entr�e - N/A
; Sortie - N/A
; Alt�r� - AF,HL

TxtOutputHookInstall
        ld a,(_txt_output)
        ld hl,(_txt_output+1)
        PUT_A txtoutputjump
        PUT_HL txtoutputjump+1

        ld hl,txtoutputhook
        PUT_HL txtoutputfaraddress
        call _kl_curr_selection
        PUT_A txtoutputfaraddress+2

        GETPTR_HL txtoutputfaraddress
        ld a,&df
        ld (_txt_output),a
        ld (_txt_output+1),hl
TxtOutputHookClearPos
        PUT_8 txtoutputpos,0
        ret

;
; D�sinstalle l'indirection de modification de la sortie Amsdos
; (sans effet si l'indirection n'avait pas �t� install�e)
;
; Entr�e - N/A
; Sortie - N/A
; Alt�r� - AF,HL

TxtOutputHookUninstall
        GET_A txtoutputjump
        or a
        ret z   ; Pas install� !
        GET_HL txtoutputjump+1
        ld (_txt_output),a
        ld (_txt_output+1),hl
        ret

;
; Indirection install�e � la place de _txt_output et qui modifie la sortie Amsdos
;
; Entr�e - A = Caract�re � afficher
; Sortie - N/A
; Alt�r� - Aucun

TxtOutputHook
        ld iy,(_dos_rom_base_address)

        di
        ex af,af'
        exx
        ld a,c
        pop de
        pop bc
        pop hl
        ex (sp),hl
        push bc
        push de
        ld c,a
        ld b,&7f
        ld de,txtoutputroutine-_txt_output-3
        add hl,de
        push hl
        exx
        ex af,af'
        ei
        ret
TxtOutputRoutine
        push hl
        push af
        push bc
        call txtoutputroutineinternal
        pop bc
        pop af
        pop hl
        ret

TxtOutputRoutineInternal
        ld hl,txtoutputmatchstring
        GET_C txtoutputpos
        ld b,0
        add hl,bc
        cp (hl)
        jr nz,txtoutputhooknomatch
        ld a,c
        cp 5    ; "Drive "
        jr z,txtoutputhookreplace
        inc c
TxtOutputHookSetPos
        PUT_C txtoutputpos
        ret
TxtOutputHookReplace
        ld hl,txtoutputreplacestring
        ld b,9  ; "Device DF"
TxtOutPutHookReplaceLoop
        push hl
        push bc
        ld a,(hl)
        call txtoutputhookjump
        pop bc
        pop hl
        inc hl
        djnz txtoutputhookreplaceloop
        jr txtoutputhookclearpos
TxtOutputHookNoMatch
        push af
        ld a,c
        or a
        call nz,txtoutputhookflushbuffer
        pop af
        call txtoutputhookjump
        ret

TxtOutputHookFlushBuffer
        ld hl,txtoutputmatchstring
TxtOutputHookFlushBufferLoop
        push hl
        ld a,(hl)
        call txtoutputhookjump
        pop hl
        inc hl
        dec c
        jr nz,txtoutputhookflushbufferloop
        jr txtoutputhooksetpos

TxtOutputHookJump
        GETPTR_HL txtoutputjump
        jp (hl)

TxtOutputMatchString
        db "Drive "
TxtOutputReplaceString
        db "Device DF"

