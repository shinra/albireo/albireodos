;
; Flags pour les API des noeuds DOS
;

; Bits
Media_Available_B   Equ 0
Media_HandleDir_B   Equ 1
Media_ReadOnly_B    Equ 2
Media_Removable_B   Equ 3
Media_StreamOnly_B  Equ 4
Media_NewAPI_B      Equ 5

; Flags
Media_Available_F   Equ 1 << media_available_b
Media_HandleDir_F   Equ 1 << media_handledir_b
Media_ReadOnly_F    Equ 1 << media_readonly_b
Media_Removable_F   Equ 1 << media_removable_b
Media_StreamOnly_F  Equ 1 << media_streamonly_b
Media_NewAPI_F      Equ 1 << media_newapi_b

